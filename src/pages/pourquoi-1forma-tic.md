## 1forma-tic.fr

**Quel est l'histoire de cette enseigne, de ce nom ?**

Historiquement, quand je me suis mis à mon compte, j'ai communiqué en utilisant l'enseigne 1000Feuilles.
Après quelques démélés judiciaires avec une agence de communication parisienne mécontente, je suis passé à 1twitif.
Sauf que s'appeller "intuitif" et avoir besoin de l'épeler à chaque fois, ça ne me va pas.

Les années passant, j'ai constaté que peu importe mon enseigne, ce que mes clients retiennent, c'est moi.
Du coup, pas besoin d'enseigne, je suis suffissamment identifiable, par mon nom, mon look, ma personnalité,
pour me passer d'un nom commercial.

Bien ! C'est donc moi que je vais mettre en avant dans ma communication professionnelle,
mais puisque je suis loin de me résumer à mon activité professionnelle,
pour donner plus de lisibilité à ceux qui me découvrent, j'ai besoin d'un espace dédié au monde pro.

C'est ainsi qu'1forma-tic.fr est né.

- Un nom qui annonce la couleur : il s'agit d'informatique.
- Un nom suffisement générique pour ne pas constituer une marque différenciante, et ainsi me mettre à
  l'abris des miasmes juridiques du droit des marques et de la propriété intellectuelle en général.
- Un nom suffisement générique pour ne pas capter les projecteurs de ce nouveau choix de communication,
        centré sur l'individu et ses activités, plutôt que sur un nom d'entreprise fatalement moins humain.
- Un nom qui s'inscrit dans la continuité des précédents, en commençant par le chiffre 1
- Un nom qui met l'accent sur la forma-tion, ce qui reflète bien le plaisir que je prends à transmettre.
- Un nom qui fait apparaître
  [TIC (Technologies de l'Information et de la Communication)](https://fr.wikipedia.org/wiki/Technologies_de_l%27information_et_de_la_communication),
  ce qui illustre la diversité de mes domaines d'interventions, car si j'ai mes spécialités,
  en tant que passionné depuis le plus jeune âge,
  j'ai eu l'occasion d'explorer bien des domaines associés à l'outil informatique.
- Un nom court, que j'espère facile à mémoriser.
