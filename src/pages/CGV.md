<%
const temps_par_mois_abonnement = [0.25,0.5,0.75,1,1.25,1.5,2,3,4,5,6,7,8,9,10];
const precision = 6; // taux de change

%>

## CGV - Conditions Générales de Vente

### Compte Client

Un compte client vous est associé.
Il contient le temps de main d'oeuvre que vous avez acheté
pour disposer de mes services.

J'y décompte le temps que je vous consacre.

### Compte solidaire

<div class="ethic">

#### Pourquoi ?

Aujourd'hui, la technologie est omniprésente,
mais tout le monde n'est pas égal face à elle.

J'aimerais réduire la fracture numérique
en rendant mes services accessibles à tous ceux qui en ont besoin,
quels que soient leurs moyens.

Pour garder mon activité viable durablement
tout en visant cet objectif d'inclusion,
je me suis inspiré de valeurs véhiculées par la
[coresponsabilité financière](https://cerclesrestauratifs.org/wiki/Coresponsabilit%C3%A9_financi%C3%A8re),
de réflexions en psychologie sociale autour du rapport au don tel qu'illustré dans [un monde meilleur](http://www.allocine.fr/film/fichefilm_gen_cfilm=28027.html)
et du fonctionnement des [cafés suspendus](https://www.solidarum.org/vivre-ensemble/generosite-de-comptoir-du-cafe-suspendu).
C'est ainsi qu'est né le compte solidaire.

</div>

#### Comment :

Le compte solidaire est un compte commun à tous les usagers.

Chacun peut y contribuer et en cas de nécessité, chacun peut y puiser.

En espérant favoriser l'équité, je suis suceptible de proposer l'usage du compte solidaire
à qui me semble en avoir besoin (en particulier les personnes en difficulté financière
qui n'osent pas demander).
De même, je me donne le droit de freiner ou refuser l'usage
du compte solidaire si certains y ont recours au-delà de ce qui me semble respectueux des autres.

Vous avez plusieurs possibilitées pour contribuer au compte solidiaire, elles sont détaillées dans la section [soutiens](#soutiens).

### Intervention sans provisions

Si vous n'avez pas ou plus de temps provisonné sur votre compte client,
le temps consommé vous sera facturé **plein tarif**.
Il sera à régler le jour même, en fin d'intervention.

#### Alternative solidaire
En cas de nécessité, vous pouvez puiser dans les provisions du **compte solidaire**
pour couvrir tout ou partie du temps que vous avez consommé
(sous réserve que le compte solidaire ne soit pas vide).

### Calcul du temps de main d'oeuvre

Tout quart d'heure entamé est dû.<br/>
*Ceci dit, si nous passons plusieurs heures ensemble, j'ai plutôt tendance
à arrondir à l'inférieur comme geste commercial.*

En cas de **déplacement**, 30 minutes minimum seront décomptées.
Je considère le temps pour me rendre chez vous comme vous étant consacré.
J'inclu donc le trajet aller dans le temps décompté (mais pas le retour).

### Soutiens

#### Pourquoi ?

Je souhaite contribuer de mon mieux à rendre ce monde meilleur pour toutes et tous,
y compris les générations futures.
Pour cela, je consacre beaucoup de mon temps aux causes qui ont du sens à mes yeux
et aux quelles j'ai l'impression de pouvoir contribuer efficacement avec mes compétences.
Le compte solidaire est un exemple parmi d'autres de mes choix engagés.

Grace à un train de vie économe, je peux consacrer beaucoup de mon temps et de mon énergie bénévolement
à faire avancer ce qui me semble utile et pertinent pour notre société, pour mieux vivre ensemble durablement,
mais j'aimerais pouvoir le faire encore plus et votre soutien financier me serait utile pour cela.

#### Comment :

Vous pouvez soutenir mes activités bénévoles ainsi que provisionner le compte solidaire comme suit :
- en souscrivant à du **soutien régulier** (don récurrent)
- en offrant le temps qui dépasserait le plafond de votre **abonnement** lorsque vous n'en faites pas usage.
- en offrant une partie du temps provisionné sur votre compte client
  *(ce qui vous sera proposé après 12 mois d'inactivité de votre compte,
  et qui sera effectué d'office 12 mois plus tard en cas d'absence de réponse de votre part).*
- par don ponctuel.


Pour chacun de ces mode de soutien, vous pouvez répartir le montant entre les usages suivants :

- Soutenir mes implications bénévoles (en m'offrant, plus particulièrement par des apports réguliers, de la visibilité financière,
c'est autant d'énergie et de temps gagnés pour assurer ma subsistance et donc utilisables
pour mes activités bénévoles).
- Provisionner le compte solidaire.

Par défaut, 2/3 du montant soutient mes activités bénévoles et 1/3 provisionne le compte solidaire.

### Formules

- **Soutien régulier :** <!--( Soutien récurrent / Goutte à goutte / Don récurrent / Abonnement de soutien )-->
  Cette formule ne vous apporte pas de bénéfice personnel (si ce n'est ma gratitude).
  Elle est idéale pour m'aider à mettre mon energie là ou elle me semble la plus utile,
  sans peur du lendemain. Merci !
  C'est aussi un excellent moyen d'allimenter le compte solidaire
  au travers des dons récurents mensuels, qui caractérisent cette formule.

  Le montant de vos soutiens est libre
  *(sauf si un intermédiaire de paiement applique des frais dissuasifs pour les petits soutiens).*
  La part attribuée au compte solidaire sera convertie en provision de temps
  à raison d'1h pour <%-htmlPrix(taux_horaire_abonnement)%>.

- **Abonnement :**
  Cette formule provisionne du temps sur votre compte client chaque mois.
  Le temps provisionné s'accumule d'un mois sur l'autre jusqu'à un plafond maximum (le surplus est converti en soutien).
  Sans engagement et non remboursable, vous pouvez changer d'abonnement ou l'interrompre à tous moment,
  le temps provisonné jusque là sera conservé sur votre compte.

  Plusieurs abonnements sont disponibles pour s'adapter à vos besoins
  (de <%=ft(temps_min_par_mois_abonnement)%> par mois
  à <%=ft(temps_max_par_mois_abonnement)%> par mois) :
  <ul>
  <%_
      for(let t of temps_par_mois_abonnement){
      //for(let t = temps_min_par_mois_abonnement; t<=temps_max_par_mois_abonnement;t+=temps_min_par_mois_abonnement){
        let max = plafond(t);
        let prix = htmlPrix(t*taux_horaire_abonnement);
        if(t===temps_min_par_mois_abonnement){
        %>
          <li>
            Pour <%-prix%> par mois, provisionne <%=ft(t)%> par mois sur votre compte client.
            S'accumule jusqu'à <%=ft(max)%>.
            <em>Ce plafond est atteint au-delà de <%=Math.floor(max/t)%> mois sans utilisation.</em>
          </li>
        <%_}else{%>
          <li>
            ( <%-prix%> : <%=ft(t)%> ) par mois, max <%=ft(max)%> <em>(en <%=Math.floor(max/t)%> mois).</em>
          </li>
  <%_}}%></ul>


  *Pour les curieux, le plafond est calculé avec la formule : `f(x)=round(x*(1+35*(10*x)^(-0.6)))`
  soit le plus en français possible :
  `plafond(temps_mensuel) = arrondi_à_l'heure_près( temps_mensuel x ( 1 + 35 x (10 x temps_mensuel)puissance(-0,6) ) )`.*

  Si vous avez besoin d'une grosse intervention le jour ou vous soucrivez à un abonnement,
  et que votre compte n'est pas déjà suffisement provisonné en temps,
  n'hésitez-pas à demander un geste commercial, j'aurais peutêtre mieux à vous proposer
  que le taux horaire **plein tarif**.

  Quel que soit l'abonnement choisi, le prix de l'heure reste le même : <%-htmlPrix(taux_horaire_abonnement)%>.


- **Forfait prépayé :**

  Ce forfait provisionne 4 heures sur votre compte client.
  Il est sans engagement et non remboursable.

  Le prix du forfait prépayé de 4h est de <%-htmlPrix(4*taux_horaire_prépayé)%> soit <%-htmlPrix(taux_horaire_prépayé)%> de l'heure.

- **Plein tarif**

  Simple et clair, je vous facture le temps que je vous ai consacré.

  Je ne recommande cependant le plein tarif qu'en dernier recours pour les raisons suivantes :
  - Il n'est pas économique, chaque 1/4h est facturé <%-htmlPrix(1/4*taux_horaire_plein_tarif)%> soit <%-htmlPrix(taux_horaire_plein_tarif)%> de l'heure.
  - Il nécessite une facturation pour chaque intervention, ce qui alourdi l'assistance à distance.

**Synthèse comparative :** pour <%-htmlPrix(4*taux_horaire_prépayé)%> vous avez
<%=ft(4*taux_horaire_prépayé/taux_horaire_plein_tarif)%> de prestation plein tarif
ou <%=ft(4*taux_horaire_prépayé/taux_horaire_prépayé)%> de forfait prépayé
ou <%=ft(4*taux_horaire_prépayé/taux_horaire_abonnement)%> d'abonnement (mensualisé à partir de <%-htmlPrix(temps_min_par_mois_abonnement*taux_horaire_abonnement)%> par mois)

### Devises acceptées


- **Ğ1** :
  La June (Ğ1) est une monnaie plus égalitaire et redistributive que l'Euro.
  Son concept de monnaie libre et son adoption progressive par une communauté grandissante
  représente pour moi le chemin le plus concret vers une réapropriation de la monnaie
  comme outil d'échange au service de chaque être humain.

  <span class="montant"><%-fm(G1(1)/G1(1),precision)%></span><span class="monnaie"><img src="img/G1June.svg" alt="Ǧ1" class="textSized"/></span>
  = <span class="montant"><%-fm(DU(1)/G1(1),precision)%></span><span class="monnaie">DU<sub><img src="img/G1June.svg" alt="Ǧ1" class="textSized"/></sub></span>
  = <span class="montant"><%-fm(MN(1)/G1(1),precision)%></span><span class="monnaie">M/N<sub><img src="img/G1June.svg" alt="Ǧ1" class="textSized"/></sub></span>
  = <span class="montant"><%-fm(Miel(1)/G1(1),precision)%></span><span class="monnaie"><img src="img/Miel.svg" alt="Miel" class="textSized"/></span>
  = <span class="montant"><%-fm(1/G1(1),precision)%></span><span class="monnaie">€</span>

- **<span class="monnaie">DU<sub><img src="img/G1June.svg" alt="Ǧ1" class="textSized"/></sub></span>** :
  Grace à la transparence qu'offre la June (Ğ1) sur son fonctionnement,
  il est possible d'utiliser d'autres unité pour exprimer un prix dans cette monnaie.

  Le DU (Dividende Universel) représente la part de monnaie ajouté quotidiennement
  au compte de chaque membre.
  Compter en DU devrait permettre d'avoir des prix plus stable dans le temps.
  En euro, évaluer vos postes de dépenses et vos revenus en nombre de RSA, depuis votre naissance
  pourrais vous donner un apperçu de la différence entre compter en € ou en RSA<sub>€</sub>.
  Il en va de même entre les <img src="img/G1June.svg" alt="Ǧ1" class="textSized"/> et les <span class="monnaie">DU<sub><img src="img/G1June.svg" alt="Ǧ1" class="textSized"/></sub></span>.

  <span class="montant"><%-fm(DU(1)/DU(1),precision)%></span><span class="monnaie">DU<sub><img src="img/G1June.svg" alt="Ǧ1" class="textSized"/></sub></span>
  = <span class="montant"><%-fm(G1(1)/DU(1),precision)%></span><span class="monnaie"><img src="img/G1June.svg" alt="Ǧ1" class="textSized"/></span>
  = <span class="montant"><%-fm(MN(1)/DU(1),precision)%></span><span class="monnaie">M/N<sub><img src="img/G1June.svg" alt="Ǧ1" class="textSized"/></sub></span>
  = <span class="montant"><%-fm(Miel(1)/DU(1),precision)%></span><span class="monnaie"><img src="img/Miel.svg" alt="Miel" class="textSized"/></span>
  = <span class="montant"><%-fm(1/DU(1),precision)%></span><span class="monnaie">€</span>

- **<span class="monnaie">M/N<sub><img src="img/G1June.svg" alt="Ǧ1" class="textSized"/></sub></span>** :
  La June (Ğ1) étant récente et sa communauté croissante,
  le <span class="monnaie">DU<sub><img src="img/G1June.svg" alt="Ǧ1" class="textSized"/></sub></span>
  représente un indicateur dont le degré d'abondance ou de rareté n'est pas encore stable.

  Certains voient dans la masse monétaire moyenne par membre (M/N : Masse monétaire / Nombre de membre)
  un référentiel plus stable dès maintenant dans son degré d'abondance ou de rareté.

  Cette une unité de mesure des prix est celle que j'ai choisi pour l'instant comme référence
  pour convertir mes prix entre euro et June.

  <span class="montant"><%-fm(MN(1)/MN(1),precision)%></span><span class="monnaie">M/N<sub><img src="img/G1June.svg" alt="Ǧ1" class="textSized"/></sub></span>
  = <span class="montant"><%-fm(G1(1)/MN(1),precision)%></span><span class="monnaie"><img src="img/G1June.svg" alt="Ǧ1" class="textSized"/></span>
  = <span class="montant"><%-fm(DU(1)/MN(1),precision)%></span><span class="monnaie">DU<sub><img src="img/G1June.svg" alt="Ǧ1" class="textSized"/></sub></span>
  = <span class="montant"><%-fm(Miel(1)/MN(1),precision)%></span><span class="monnaie"><img src="img/Miel.svg" alt="Miel" class="textSized"/></span>
  = <span class="montant"><%-fm(1/MN(1),precision)%></span><span class="monnaie">€</span>

- **Miel** :
  Monnaie Complémentaire Locale (MCL) utilisée à Bordeaux.
  Bien qu'assujettie à l'Euro, la Miel a le mérite de questionner l'impuissance citoyenne
  face aux rapports de pouvoir entretenus par les monnaies d'état.
  Elle dispose également d'une charte éthique
  que je souhaite soutenir en acceptant cette devise.

  <span class="montant"><%-fm(Miel(1)/Miel(1),precision)%></span><span class="monnaie"><img src="img/Miel.svg" alt="Miel" class="textSized"/></span>
  = <span class="montant"><%-fm(G1(1)/Miel(1),precision)%></span><span class="monnaie"><img src="img/G1June.svg" alt="Ǧ1" class="textSized"/></span>
  = <span class="montant"><%-fm(DU(1)/Miel(1),precision)%></span><span class="monnaie">DU<sub><img src="img/G1June.svg" alt="Ǧ1" class="textSized"/></sub></span>
  = <span class="montant"><%-fm(MN(1)/Miel(1),precision)%></span><span class="monnaie">M/N<sub><img src="img/G1June.svg" alt="Ǧ1" class="textSized"/></sub></span>
  = <span class="montant"><%-fm(1/Miel(1),precision)%></span><span class="monnaie">€</span>


- **Euro** :
  Monnaie banquaire utilisée en France et dans de nombreux pays d'Europe
  comme monnaie institutionnelle. Par ses règles de fonctionnment (notament d'émission),
  elle crée, entretient et accentue des rapports de pouvoir oppressifs entre
  acteurs économiques au détriment de la grande majorité des êtres humains
  qui vivent là où l'euro est en usage (ou toute autre monnaie similaire telle que le dollar).

  Cependant, cette monnaie étant largement dominante là ou je vie,
  c'est elle que j'utilise le plus au quotidien pour assurer ma subsistence.
  Je l'accepte donc avec enthousiasme, par nécessité.

  <span class="montant"><%-fm(1,precision)%></span><span class="monnaie">€</span>
  = <span class="montant"><%-fm(G1(1),precision)%></span><span class="monnaie"><img src="img/G1June.svg" alt="Ǧ1" class="textSized"/></span>
  = <span class="montant"><%-fm(DU(1),precision)%></span><span class="monnaie">DU<sub><img src="img/G1June.svg" alt="Ǧ1" class="textSized"/></sub></span>
  = <span class="montant"><%-fm(MN(1),precision)%></span><span class="monnaie">M/N<sub><img src="img/G1June.svg" alt="Ǧ1" class="textSized"/></sub></span>
  = <span class="montant"><%-fm(Miel(1),precision)%></span><span class="monnaie"><img src="img/Miel.svg" alt="Miel" class="textSized"/></span>

### Modes de paiement

**Modes de paiement préférés pour les abonnements et soutiens réguliers :**
- (Euro) Virement automatique : sans frais pour moi, et selon votre banque, en général sans frais pour vous.
  Mon IBAN : FR76 1090 7000 0132 0196 9604 181
- (Euro) Prélèvement automatique SEPA
- (Ğ1) Virement j'espère bientôt automatique. Ma clef publique : 2sZF6j2PkxBDNAqUde7Dgo5x3crkerZpQ4rBqqJGn8QT


**Modes de paiement privilégiés pour les paiements unique :**

- Espèce (Miel, Euro)
- Virement (Ğ1, Euro)
  - Ğ1 : 2sZF6j2PkxBDNAqUde7Dgo5x3crkerZpQ4rBqqJGn8QT
  - € IBAN : FR76 1090 7000 0132 0196 9604 181

**Autres modes de paiement acceptés mais déconseillé :**

Ces modes de paiement entrainent des frais importants (jusqu'à 10%)
ou me demande du temps pour aller les encaisser, je préfère donc les éviter,
sans pour autant les refuser catégoriquement.

- Chèque (Euro) A l'ordre de : Millicent Billette de Villemeur ~ 1forma-tic.fr
- CB (Euro) via [OpenCollective](https://opencollective.com/soutien-1000i100),
  Paypal, Liberapay, Tipeee, uTip, Patreon ou Github Sponsors.
