# [1forma-tic.fr](https://1forma-tic.fr)

## Environnement local
````bash
git clone git@framagit.org:1000i100/1forma-tic.fr.git
cd 1forma-tic.fr
npm install
````
Afficher le site : `public/index.html`

Regénérer le site au moindre changement dans `src/` : `npm run watch`
