const https = require('https');
const fs = require("fs");

async function main(){
  const numeroDuDernierBlocAvecDU = (await get("https://duniter.g1.1000i100.fr/blockchain/with/ud")).result.blocks.pop();
  const dernierBlocAvecDU = (await get(`https://duniter.g1.1000i100.fr/blockchain/block/${numeroDuDernierBlocAvecDU}`));


  const G1parDU = dernierBlocAvecDU.dividend/100;
  const MsurN_DU = Math.round(dernierBlocAvecDU.monetaryMass/100 / dernierBlocAvecDU.membersCount / G1parDU) ; // 485 au 24/12/2019,  310 au 01/01/2019;

  fs.mkdirSync('generated.ejs',{'recursive':true});
  fs.writeFileSync(`generated.ejs/G1Data.js`, `
const G1parDU =  ${G1parDU};
const MsurN_DU = ${MsurN_DU}; // 485 au 24/12/2019,  310 au 01/01/2019;
`);
}
main();

async function get(url){
  return new Promise((resolve, reject) =>{
    https.get(url, (resp) => {
      let data = '';
      resp.on('data', (chunk) => data += chunk);
      resp.on('end', () => resolve(JSON.parse(data)) );
    }).on("error", (err) => reject(err) );
  });
//return JSON.parse( request('GET', 'http://example.com').getBody() );
}
