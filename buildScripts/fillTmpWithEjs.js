const fs = require("fs");
const yaml = require("js-yaml");
const md = require('marked');
md.setOptions({headerIds: true, xhtml: true});

// Usage
if(process.argv.length !== 4){
    const programName = process.argv[1].split('/').pop();
    console.error(`\nUsage : ${programName} srcFolder targetFolder`);
    process.exit(1);
}

// args
let srcFolder = process.argv[2];
let targetFolder = process.argv[3];

// converter rules
const actions = {
    'ejs': (str)=>str,
    'md': (str)=>md(str.replace(/<%/g,'<![CDATA[').replace(/%>/g,']]>')).replace(/<!\[CDATA\[/g,'<%').replace(/]]>/g,'%>'),
    'yml': (str)=>yaml.safeLoad(str),
};

const constAndUtilityFunc = `
${fs.readFileSync(`generated.ejs/G1Data.js`, 'utf8')}
${fs.readFileSync(`src/fragments/devisesLib.js`, 'utf8')}
${fs.readFileSync(`node_modules/latinize-to-ascii/latinize.js`, 'utf8')}
`;

// processor
function recurciveConverter(srcFolder,targetFolder){
    fs.mkdirSync(targetFolder,{'recursive':true});
    fs.readdirSync(srcFolder).forEach((fileName)=>{
        if(fs.statSync(`${srcFolder}/${fileName}`).isDirectory())
            return recurciveConverter(`${srcFolder}/${fileName}`,`${targetFolder}/${fileName}`);
        const fPart = fileName.split('.');
        const fileExtension = fPart.pop();
        const baseName = fPart.join('.');
        if(actions[fileExtension]){
            const srcFileContent = fs.readFileSync(`${srcFolder}/${fileName}`, 'utf8');
            const convertedFileContent = actions[fileExtension](srcFileContent);
            if(fileExtension==='yml') fs.writeFileSync(`${targetFolder}/${baseName}.json.ejs`, `${JSON.stringify(convertedFileContent)}`);
            else fs.writeFileSync(`${targetFolder}/${baseName}.ejs`, `<% ${constAndUtilityFunc}%>${convertedFileContent}`);
        } // else console.log(`ignoring : ${srcFolder}/${fileName}`);
    });
}
recurciveConverter(srcFolder,targetFolder);
