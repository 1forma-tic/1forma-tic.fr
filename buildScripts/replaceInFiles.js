const now = (new Date).toISOString();
const fs = require("fs");

replacesInFiles(["generated.public/index.html","generated.public/sitemap.xml", "generated.public/humans.txt"],{"now":now});

function replacesInFiles(fileList,replaces) {
    for(let fileName of fileList) replacesInFile(fileName,replaces);
}
function replacesInFile(fileName,replaces) {
    const regex = new RegExp(Object.keys(replaces).map(key=>`\\{build\\:${key}\\}`).join('|'),'g');
    const replacements = {};
    for (let k in replaces) replacements[`{build:${k}}`]=replaces[k];

    let fileContent = fs.readFileSync(fileName,"utf8");
    fileContent = fileContent.replace(regex,match=>replacements[match]);
    fs.writeFileSync(fileName,fileContent,"utf8");
}
